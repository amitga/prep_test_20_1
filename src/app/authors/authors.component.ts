import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  typesOfAuthors: object[] = [{id:1,name:'Yuval Noah arri'},{id:2,name:'Ami Rubinger'},{id:3,name:'J.K Rolling'},{id:4,name:'Dvora Omer'},{id:5,name:'Netanel Eliashiv'}];
    
    name;
    id;
  
    constructor(private route: ActivatedRoute) { }
  ngOnInit() {
    this.name = this.route.snapshot.params.name;
    this.id= this.route.snapshot.params.id;
  }

}
